<?php
    function ubah_huruf($string){
       $huruf = "abcdefghijklmnopqrstuvwxyz";
       $output = "";
       for ($i=0; $i < strlen($string); $i++) { 
           $posisi = strrpos($huruf, $string[$i]);
           $output .= substr($huruf, $posisi + 1, 1);
       } 
       return $output. "<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>